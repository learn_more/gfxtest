#ifndef DX9RENDER_H
#define DX9RENDER_H

#include "../baseRenderPlugin.h"

struct dx9Info;

class dx9Render : public baseRender
{
public:
	dx9Render();
	virtual ~dx9Render();

	virtual bool isCapable( RenderMode ) const { return true; }
	virtual void onChangeRenderMode();

	virtual bool init( HWND hWnd );
	
	virtual void* modifyData( void* data );
	virtual void frameTick( void* data, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] );
	virtual void flip( void* );
	
	virtual void cleanup();
private:
	dx9Render& operator = ( const dx9Render& );
	dx9Info& info;
};


#endif //#ifndef DX9RENDER_H