#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "dx9Render.h"

#include <d3d9.h>
#include <d3dx9.h>

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

struct vertex2d { float x, y, z, rhw; DWORD color; };
#define fvf2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)

struct vertex3d { float x, y, z; DWORD color; };
#define fvf3D (D3DFVF_XYZ | D3DFVF_DIFFUSE)

extern pluginHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createRender( pluginHelper* help, int version )
{
	if( PLUGINRENDER_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new dx9Render();
}

extern "C" __declspec(dllexport)
void destroyRender( basePlugin* render )
{
	delete render;
}

struct dx9Info
{
	LPDIRECT3D9 d3d;
	LPDIRECT3DDEVICE9 dev;
	LPDIRECT3DVERTEXBUFFER9 v_buffer;
	LPD3DXFONT d3dFont;
	LPD3DXLINE d3dMouseLines[2];
};


dx9Render::dx9Render()
:info(*(new dx9Info()))
{
	memset( &info, 0, sizeof(dx9Info) );
}

dx9Render::~dx9Render()
{
	delete &info;
}

bool dx9Render::init( HWND hWnd )
{
	info.d3d = Direct3DCreate9( D3D_SDK_VERSION );
	D3DDISPLAYMODE d3ddm;
	if( FAILED(info.d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)) ) {
		return false;
	}

	D3DPRESENT_PARAMETERS d3dpp = {0};
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.BackBufferFormat = d3ddm.Format;
	d3dpp.BackBufferWidth = Width;
	d3dpp.BackBufferHeight = Height;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; 

	info.d3d->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &info.dev );
	info.dev->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
	info.dev->SetRenderState( D3DRS_LIGHTING, FALSE );

	D3DXCreateLine(info.dev, &info.d3dMouseLines[0]);
	D3DXCreateLine(info.dev, &info.d3dMouseLines[1]);

	onChangeRenderMode();
	return true;
}

void dx9Render::onChangeRenderMode()
{
	safeRelease( info.v_buffer );
	D3DCOLOR objCol = D3DCOLOR_XRGB( Object.r, Object.g, Object.b );
	if( ActiveMode == render2D ) {
		vertex2d vertices[] =
		{
			{ Width/2.f, (float)Offset, 0.5f, 1.0f, objCol, },
			{ (float)(Width-Offset), (float)(Height-Offset), 0.5f, 1.0f, objCol, },
			{ (float)(Offset), (float)(Height-Offset), 0.5f, 1.0f, objCol, },
		};
		info.dev->CreateVertexBuffer( sizeof(vertices), 0, fvf2D, D3DPOOL_MANAGED, &info.v_buffer, NULL );
		VOID* pVoid;
		info.v_buffer->Lock( 0, 0, (void**)&pVoid, 0 );
		memcpy( pVoid, vertices, sizeof(vertices) );
		info.v_buffer->Unlock();
	} else {
		vertex3d vertices[] =
		{
			// Top Face
			{-5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(0, 0, 255),},  // Vertex 0 - Blue
			{-5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(255, 0, 0),},   // Vertex 1 - Red
			{5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),},   // Vertex 2 - Red
			{5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),},    // Vertex 3 - Green

			// Face 1
			{-5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),}, // Vertex 4 - Red
			{-5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(0, 0, 255),},  // Vertex 5 - Blue
			{5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(0, 255, 0),},  // Vertex 6 - Green
			{5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),},   // Vertex 7 - Red

			// Face 2
			{5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 0, 255),},   // Vertex 8 - Blue
			{5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),},    // Vertex 9 - Green

			// Face 3
			{-5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),}, //Vertex 10 - Green
			{-5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(255, 0, 0),}, //Vertex 11 - Red

			// Face 4
			{-5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),}, //Vertex 12 - Red
			{-5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(0, 0, 255),}, //Vertex 13 - Blue

			// Bottom Face
			{5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(0, 255, 0),}, //Vertex 14 - Green
			{5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 0, 255),}, //Vertex 15 - Blue
			{-5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),}, //Vertex 16 - Red
			{-5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),}, //Vertex 17 - Green
		};

		info.dev->CreateVertexBuffer( sizeof(vertices), 0, fvf3D, D3DPOOL_MANAGED, &info.v_buffer, NULL );
		VOID* pVoid;
		info.v_buffer->Lock( 0, 0, (void**)&pVoid, 0 );
		memcpy( pVoid, vertices, sizeof(vertices) );
		info.v_buffer->Unlock();
	}

	// Create font to draw fps while rendering.
	D3DXCreateFont(info.dev, 16, 0, FW_BOLD, 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DT_LEFT | DT_TOP, L"Microsoft Sans Serif", &info.d3dFont); 
}

void* dx9Render::modifyData( void* )
{
	return info.dev;
}

void dx9Render::frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] )
{
	LPDIRECT3DDEVICE9 dev = info.dev;
	dev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB( Background.r, Background.g, Background.b ), 1.0f, 0);

	dev->BeginScene();

	if( ActiveMode == render2D ) {
		dev->SetFVF( fvf2D );
		dev->SetStreamSource( 0, info.v_buffer, 0, sizeof(vertex2d) );
		dev->DrawPrimitive( D3DPT_TRIANGLELIST, 0, 1 );
	} else {
		//rotation
		D3DXMATRIX matWorld, matWorldX, matWorldY, matWorldZ;
		D3DXMatrixRotationX(&matWorldX, 0);
		D3DXMatrixRotationY(&matWorldY, tick/400.0f);
		D3DXMatrixRotationZ(&matWorldZ, 0);

		D3DXMatrixMultiply(&matWorld, &matWorldX, &matWorldY);
		D3DXMatrixMultiply(&matWorld, &matWorld, &matWorldZ);

		dev->SetTransform( D3DTS_WORLD, &matWorld );
		//camera:
		D3DXMATRIX matView;
		D3DXMatrixLookAtLH(&matView, &D3DXVECTOR3(0.0f, 10.0f,-30.0f), //Cam
			&D3DXVECTOR3(0.0f, 0.0f, 0.0f),  //Look At
			&D3DXVECTOR3(0.0f, 1.0f, 0.0f)); //Up

		dev->SetTransform( D3DTS_VIEW, &matView );

		//perspective
		D3DXMATRIX matProj;
		D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, 1.0f, 1.0f, 500.0f );
		dev->SetTransform(D3DTS_PROJECTION, &matProj);

		//yay, we get to draw finally
		dev->SetStreamSource( 0, info.v_buffer, 0, sizeof(vertex3d) );
		dev->SetFVF( fvf3D );
		dev->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2 );  // Top
		dev->DrawPrimitive( D3DPT_TRIANGLESTRIP, 4, 8 );  // Sides
		dev->DrawPrimitive( D3DPT_TRIANGLESTRIP, 14, 2 ); // Bottom
	}

	// Draw FPS on screen
	const wchar_t* buf = helper->va(L"fps: %5.02f", fps);
	RECT rc = {10, 10, 0, 0};
	LPD3DXFONT fnt = info.d3dFont;
	rc.top += fnt->DrawTextW(NULL, buf, -1, &rc, DT_NOCLIP, D3DCOLOR_XRGB(255, 255, 255));
	while( gfxInfo && *gfxInfo ) {
		rc.top += fnt->DrawTextW(NULL, *gfxInfo, -1, &rc, DT_NOCLIP, D3DCOLOR_XRGB(255, 255, 255));
		++gfxInfo;
	}

	// Draw mouse position on screen.
	D3DXVECTOR2 lines1[] = { D3DXVECTOR2(0.0f, (float)mousePos[1]), D3DXVECTOR2((float)this->Width, (float)mousePos[1]) };
	D3DXVECTOR2 lines2[] = { D3DXVECTOR2((float)mousePos[0], 0.0f), D3DXVECTOR2((float)mousePos[0], (float)this->Height) };
	info.d3dMouseLines[0]->Begin();
	info.d3dMouseLines[0]->Draw(lines1, 2, 0xFF000000);
	info.d3dMouseLines[0]->End();

	info.d3dMouseLines[1]->Begin();
	info.d3dMouseLines[1]->Draw(lines2, 2, 0xFF000000);
	info.d3dMouseLines[1]->End();

	dev->EndScene();
}

void dx9Render::flip( void* )
{
	info.dev->Present(NULL, NULL, NULL, NULL);
}

void dx9Render::cleanup()
{
	safeRelease(info.d3dMouseLines[0]);
	safeRelease(info.d3dMouseLines[1]);
	safeRelease(info.d3dFont);
	safeRelease( info.v_buffer );
	safeRelease( info.dev );
	safeRelease( info.d3d );
}
