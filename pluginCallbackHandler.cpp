#include <Windows.h>
#include <string>
#include <vector>
#include "baseRenderPlugin.h"
#include "baseInputPlugin.h"
#include "plugin.h"
#include "pluginContext.h"

//main.cpp
bool updateAllFromCommandline( const wchar_t* arg );
void destroyMainWindow();

//do not EVER touch these strings after they are initialized....
std::wstring renderList;
std::wstring inputList;
std::wstring resolutionList;

extern pluginContext<baseRender> renderer;
extern pluginContext<baseInput> input;

extern ResolutionDesc resoTable[];
extern size_t resoTableSize;

extern "C" __declspec(dllexport)
int __stdcall testControl(int action, void* arg)
{
	switch( (pluginAction_e)action ) {
		case pa_LoadModule:
			return updateAllFromCommandline( (const wchar_t*)arg ) ? 0 : 1;
		case pa_Reload:
			destroyMainWindow();
			return 0;
		case pa_Renderlist:
			*static_cast<const wchar_t**>(arg) = renderList.c_str();
			return 0;
		case pa_Inputlist:
			*static_cast<const wchar_t**>(arg) = inputList.c_str();
			return 0;
		case pa_Resolutionlist:
			*static_cast<const wchar_t**>(arg) = resolutionList.c_str();
			return 0;
		default:
			return -1;
	}
}

void appendString( std::wstring& str, const std::wstring& src )
{
	str.append( src );
	str.resize( str.size() + 1, 0 );
}

void updateLists()
{
	for( size_t n = 0; n < renderer.all.size(); ++n ) {
		appendString( renderList, renderer.all[n]->name() );
	}
	renderList.resize( renderList.size() + 1, 0 );
	for( size_t n = 0; n < input.all.size(); ++n ) {
		appendString( inputList, input.all[n]->name() );
	}
	inputList.resize( inputList.size() + 1, 0 );
	for( size_t n = 0; n < resoTableSize; ++n ) {
		wchar_t buf[50];
		swprintf_s( buf, L"%d x %d", resoTable[n].w, resoTable[n].h );
		appendString( resolutionList, buf );
	}
	resolutionList.resize( resolutionList.size() + 1, 0 );
}
