#ifndef DX11RENDER_H
#define DX11RENDER_H

#include "../baseRenderPlugin.h"

struct dx11Info;

class dx11Render : public baseRender
{
public:
	dx11Render();
	virtual ~dx11Render();

	virtual bool isCapable( RenderMode ) const { return true; }
	virtual void onChangeRenderMode();

	virtual bool init( HWND hWnd );
	
	virtual void frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] );
	virtual void flip( void* );
	
	virtual void cleanup();
private:
	dx11Render& operator = ( const dx11Render& );
	dx11Info* info;

	__declspec(noinline) const bool GFXTestSwitchModeTo2D();
	__declspec(noinline) const bool GFXTestSwitchModeTo3D();

	// Aligns an address in memory to the specific boundary.
	inline void AlignPointer(DWORD_PTR* Address, const DWORD Boundary)
	{
		if (Boundary > 0)
		{
			if ((*Address % Boundary) > 0)
			{
				const DWORD_PTR tmp = *Address;
				*Address = (tmp + Boundary) - (tmp % Boundary);
			}
		}
	};
};

#endif //#ifndef DX9RENDER_H