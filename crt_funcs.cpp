//merge the .CRT section in the .rdata section
//	contains static initializers
//	this prevents the warning about static initializers..
#pragma comment(linker, "/merge:.CRT=.rdata")

#ifndef _WIN64
#include <Windows.h>
#include <string>

#ifndef _DEBUG

#ifndef _WIN64
	#pragma comment(lib, "msvcrt86.lib")
#endif

void __cdecl _Throw(const stdext::exception& ex)
{
	MessageBoxA( NULL, ex.what(), "Exception", MB_OK | MB_ICONERROR );
	ExitProcess( 0xffffffff );
}

typedef void (__cdecl *_Prhand)(const stdext::exception&);
_Prhand std::_Raise_handler = _Throw;

__declspec(noreturn)
void __cdecl std::_Xout_of_range(const char * _Message)
{
	_THROW_NCEE(std::out_of_range, _Message);
}

__declspec(noreturn)
void __cdecl std::_Xlength_error(const char * _Message)
{
	_THROW_NCEE(std::out_of_range, _Message);
}

//arrays of static initializers as expected by the c runtime.
//	we run trough them calling them one by one.
//	list can contain null items, but it's not the end of the list.
//	a = begin, z = end of the list.
//	code proudly stolen from crt0dat.c

typedef void (__cdecl *_PVFV)(void);
typedef int  (__cdecl *_PIFV)(void);

extern "C"
{
extern _PIFV __xi_a[];
extern _PIFV __xi_z[];    // C initializers
extern _PVFV __xc_a[];
extern _PVFV __xc_z[];    // C++ initializers

extern _PVFV *__onexitbegin = (_PVFV*)-1;	//so the runtime knows we are an exe.
};

void __cdecl _initterm ( _PVFV * pfbegin, _PVFV * pfend )
{
	while ( pfbegin < pfend ) {
		if ( *pfbegin != NULL ) {
			(**pfbegin)();
		}
		++pfbegin;
	}
}

int __cdecl _initterm_e( _PIFV * pfbegin, _PIFV * pfend )
{
	int ret = 0;
	while ( pfbegin < pfend  && ret == 0) {
		if ( *pfbegin != NULL ) {
			ret = (**pfbegin)();
		}
		++pfbegin;
	}
	return ret;
}


void init_crt_shit()
{
	_initterm_e( __xi_a, __xi_z );
	_initterm( __xc_a, __xc_z );
}


#else
void init_crt_shit()
{
	//no op
}
#endif
#endif