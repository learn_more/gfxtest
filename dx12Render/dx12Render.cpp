#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "dx12Render.h"
#include <d3d12.h>
#include <dxgi1_5.h>
#include <d3dcompiler.h>
#include "d3dx12.h"
#include "TextRenderer.h"

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

extern pluginHelper* helper;

#define D3D12_BUFFERCOUNT 2

char vsHLSL[] = "struct PixelInputType { float4 position : SV_POSITION; float4 color : COLOR; }; struct VertexInputType { float4 position : POSITION; float4 color : COLOR; }; PixelInputType VS( VertexInputType input ) { PixelInputType output; output.position = input.position; output.color = input.color; return output; }";
char psHLSL[] = "struct PixelInputType { float4 position : SV_POSITION; float4 color : COLOR; }; float4 PS( PixelInputType input ) : SV_TARGET { return input.color; }";

char vsHLSL3D[] = "cbuffer ConstantBuffer : register( b0 ) { matrix World; matrix View;	matrix Projection; } struct VS_OUTPUT { float4 Pos : SV_POSITION; float4 Color : COLOR0; }; VS_OUTPUT VS( float4 Pos : POSITION, float4 Color : COLOR ) { VS_OUTPUT output = (VS_OUTPUT)0; output.Pos = mul( Pos, World ); output.Pos = mul( output.Pos, View ); output.Pos = mul( output.Pos, Projection ); output.Color = Color; return output; }";
char psHLSL3D[] = "struct VS_OUTPUT { float4 Pos : SV_POSITION; float4 Color : COLOR0; }; float4 PS( VS_OUTPUT input ) : SV_Target { return input.Color; }";

//char vsTextDrawingHLSL[] = "struct VS_INPUT { float4 pos : POSITION; float4 texCoord: TEXCOORD; float4 color: COLOR; }; struct VS_OUTPUT { float4 pos: SV_POSITION; float4 color: COLOR; float2 texCoord: TEXCOORD; }; VS_OUTPUT VS(VS_INPUT input, uint vertexID : SV_VertexID) { VS_OUTPUT output; float2 uv = float2(vertexID & 1, (vertexID >> 1) & 1); output.pos = float4(input.pos.x + (input.pos.z * uv.x), input.pos.y - (input.pos.w * uv.y), 0, 1); output.color = input.color; output.texCoord = float2(input.texCoord.x + (input.texCoord.z * uv.x), input.texCoord.y + (input.texCoord.w * uv.y)); return output; }";
//char psTextDrawingHLSL[] = "Texture2D t1 : register(t0); SamplerState s1 : register(s0); struct VS_OUTPUT { float4 pos: SV_POSITION; float4 color: COLOR; float2 texCoord: TEXCOORD; }; float4 PS(VS_OUTPUT input) : SV_TARGET { return float4(input.color.rgb, input.color.a * t1.Sample(s1, input.texCoord).a); }";

extern "C" __declspec(dllexport)
basePlugin* createRender(pluginHelper* help, int version)
{
	if (PLUGINRENDER_VERSION != version) {
		return NULL;
	}
	helper = help;
	return new dx12Render();
}

extern "C" __declspec(dllexport)
void destroyRender(basePlugin* render)
{
	delete render;
}

struct MatrixBufferType
{
	XMMATRIX world;
	XMMATRIX view;
	XMMATRIX projection;

	// The matrices struct must be aligned to 256 bytes.
	unsigned char __256Padding[64];
};

struct Vertex
{
	XMFLOAT3 position;
	XMFLOAT4 color;
};

struct dx12Info
{
	// DirectX 12 state objects.
	D3D12_VIEWPORT d3dViewport;
	D3D12_RECT d3dScissorRect;
	ID3D12Device* d3dDevice;
	ID3D12CommandQueue* d3dCommandQueue;
	IDXGISwapChain3* d3dSwapChain;
	ID3D12DescriptorHeap* d3dRenderTargetViewHeap;
	ID3D12DescriptorHeap* d3dMainDescriptorHeap;
	ID3D12Resource* d3dBackBufferRenderTarget[D3D12_BUFFERCOUNT];
	ID3D12CommandAllocator* d3dCommandAllocator;
	ID3D12GraphicsCommandList* d3dCommandList;
	ID3D12PipelineState* d3dPipelineState;
	ID3D12PipelineState* d3dLinesPipelineState;
	//ID3D12PipelineState* d3dTextPipelineState;
	ID3D12Fence* d3dFence[D3D12_BUFFERCOUNT];
	ID3D12RootSignature* d3dRootSignature;
	ID3D12Resource* d3dVertexBuffer;
	ID3D12Resource* d3dIndexBuffer;
	ID3D12Resource* d3dLineVerticesBuffer;
	ID3D12Resource* d3dMatrixConstantBuffer[D3D12_BUFFERCOUNT];
	//ID3D12Resource* d3dTextVertexBuffer[D3D12_BUFFERCOUNT];
	D3D12_VERTEX_BUFFER_VIEW d3dvertexBufferView;
	D3D12_INDEX_BUFFER_VIEW d3dIndexBufferView;
	D3D12_VERTEX_BUFFER_VIEW d3dLinesVertexBufferView;
	//D3D12_VERTEX_BUFFER_VIEW d3dTextVertexBufferView[D3D12_BUFFERCOUNT];

	// Rotation and transformation matrices.
	XMMATRIX viewMatrix;
	XMMATRIX worldMatrix;
	XMMATRIX projectionMatrix;
	MatrixBufferType matrices3D;

	// Text drawing variables.
	//Font* ArialFont;
	//unsigned int MaxNumTextCharacters;
	//unsigned int SRVHandleSize;
	//BYTE* TextVertexPointer[D3D12_BUFFERCOUNT];

	float ClearColor[4];
	float screenAspect;

	unsigned int BufferCount;
	unsigned int RenderTargetViewDescriptorSize;

	unsigned __int64 FenceValue[D3D12_BUFFERCOUNT];
	HANDLE FenceEvent;
	unsigned int FrameIndex;
};

void* originalHeapPtr = NULL;

// -------------------------------------------------------------------------------------------------------------

const bool dx12Render::GFXTestSwitchModeTo2D()
{
	ID3DBlob* vertexShader;
	ID3DBlob* pixelShader;
	ID3DBlob* error = NULL;

	// Compile the vertex shader from the HLSL strings defined on top.
#ifdef _DEBUG
	if (FAILED(D3DCompile(vsHLSL, strlen(vsHLSL), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vertexShader, &error)) && error)
#else
	if (FAILED(D3DCompile(vsHLSL, strlen(vsHLSL), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &vertexShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Compile the pixel shader from the HLSL strings defined on top.
#ifdef _DEBUG
	if (FAILED(D3DCompile(psHLSL, strlen(psHLSL), NULL, NULL, NULL, "PS", "ps_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &pixelShader, &error)) && error)
#else
	if (FAILED(D3DCompile(psHLSL, strlen(psHLSL), NULL, NULL, NULL, "PS", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &pixelShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Define the vertex input layout.
	D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	// Describe and create the graphics pipeline state object (PSO).
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc;
	memset(&psoDesc, 0, sizeof(psoDesc));
	
	psoDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
	psoDesc.pRootSignature = info->d3dRootSignature;
	psoDesc.VS.pShaderBytecode = vertexShader->GetBufferPointer();
	psoDesc.VS.BytecodeLength = vertexShader->GetBufferSize();
	psoDesc.PS.pShaderBytecode = pixelShader->GetBufferPointer();
	psoDesc.PS.BytecodeLength = pixelShader->GetBufferSize();
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;

	// Create the graphics pipeline.
	HRESULT h = info->d3dDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&info->d3dPipelineState));
	if (FAILED(h))
	{
		return false;
	}

	// Create the graphics pipeline for drawing the lines. The only thing that has changed in the descriptor object is the vertex shader and the primitive topology.
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
	if (FAILED(info->d3dDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&info->d3dLinesPipelineState))))
	{
		return false;
	}

	// Create a command list for the graphics pipeline.
	if (FAILED(info->d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, info->d3dCommandAllocator, info->d3dPipelineState, IID_PPV_ARGS(&info->d3dCommandList))))
	{
		return false;
	}

	// The frame loop expects the command list to be closed. Let's close it for now.
	info->d3dCommandList->Close();

	// Release buffers used for compiled shaders.
	safeRelease(error);
	safeRelease(vertexShader);
	safeRelease(pixelShader);

	// Divide color values by 255 to fit the D3D12 color values.
	const float r11 = (float)Object.r / 0xFF;
	const float g11 = (float)Object.g / 0xFF;
	const float b11 = (float)Object.b / 0xFF;

	// Create a vertex buffer with the 2D primitive that is going to be shown.
	Vertex vertexData[] = 
	{
		{ XMFLOAT3(-0.7f, -0.7f, 0.0f), XMFLOAT4(r11, g11, b11, 1.0f) },
		{ XMFLOAT3(0.0f, 0.7f, 0.0f), XMFLOAT4(r11, g11, b11, 1.0f) },
		{ XMFLOAT3(0.7f, -0.7f, 0.0f), XMFLOAT4(r11, g11, b11, 1.0f) }
	};

	const unsigned int vertexDataSize = sizeof(vertexData);

	// Upload the vertex buffer to the GPU using the default heap usage.
	if (FAILED(info->d3dDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(vertexDataSize), D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&info->d3dVertexBuffer))))
	{
		return false;
	}

	// Upload the vertex buffer to the GPU.
	UINT8* pVertexDataBegin;
	D3D12_RANGE readRange;
	readRange.Begin = 0;
	readRange.End = 0;
	info->d3dVertexBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexDataBegin));
	memcpy(pVertexDataBegin, vertexData, vertexDataSize);
	info->d3dVertexBuffer->Unmap(0, NULL);

	// Create a vertex buffer view.
	info->d3dvertexBufferView.BufferLocation = info->d3dVertexBuffer->GetGPUVirtualAddress();
	info->d3dvertexBufferView.StrideInBytes = sizeof(Vertex);
	info->d3dvertexBufferView.SizeInBytes = vertexDataSize;

	// Wait for the setup to complete and start the rendering.
	this->WaitForPreviousFrame();
	return true;
}

const bool dx12Render::GFXTestSwitchModeTo3D()
{
	ID3DBlob* vertexShader;
	ID3DBlob* pixelShader;
	ID3DBlob* error = NULL;

	// Compile the vertex shader from the HLSL strings defined on top.
#ifdef _DEBUG
	if (FAILED(D3DCompile(vsHLSL3D, strlen(vsHLSL3D), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vertexShader, &error)) && error)
#else
	if (FAILED(D3DCompile(vsHLSL3D, strlen(vsHLSL3D), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &vertexShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Compile the pixel shader from the HLSL strings defined on top.
#ifdef _DEBUG
	if (FAILED(D3DCompile(psHLSL3D, strlen(psHLSL3D), NULL, NULL, NULL, "PS", "ps_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &pixelShader, &error)) && error)
#else
	if (FAILED(D3DCompile(psHLSL3D, strlen(psHLSL3D), NULL, NULL, NULL, "PS", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &pixelShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Define the vertex input layout.
	D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	// Describe and create the graphics pipeline state object (PSO).
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc;
	memset(&psoDesc, 0, sizeof(psoDesc));

	psoDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
	psoDesc.pRootSignature = info->d3dRootSignature;
	psoDesc.VS.pShaderBytecode = vertexShader->GetBufferPointer();
	psoDesc.VS.BytecodeLength = vertexShader->GetBufferSize();
	psoDesc.PS.pShaderBytecode = pixelShader->GetBufferPointer();
	psoDesc.PS.BytecodeLength = pixelShader->GetBufferSize();
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState.DepthEnable = FALSE;
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;

	// Create the graphics pipeline.
	if (FAILED(info->d3dDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&info->d3dPipelineState))))
	{
		return false;
	}

	// Create a command list for the graphics pipeline.
	if (FAILED(info->d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, info->d3dCommandAllocator, info->d3dPipelineState, IID_PPV_ARGS(&info->d3dCommandList))))
	{
		return false;
	}

	// The frame loop expects the command list to be closed. Let's close it for now.
	info->d3dCommandList->Close();

	// Compile the vertex shader for 2D shading as well. We need this for drawing the mouse position lines without transformations.
#ifdef _DEBUG
	if (FAILED(D3DCompile(vsHLSL, strlen(vsHLSL), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vertexShader, &error)) && error)
#else
	if (FAILED(D3DCompile(vsHLSL, strlen(vsHLSL), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &vertexShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Create the graphics pipeline for drawing the lines. The only thing that has changed in the descriptor object is the vertex shader and the primitive topology.
	psoDesc.VS.pShaderBytecode = vertexShader->GetBufferPointer();
	psoDesc.VS.BytecodeLength = vertexShader->GetBufferSize();
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
	if (FAILED(info->d3dDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&info->d3dLinesPipelineState))))
	{
		return false;
	}

	// Release buffers used for compiled shaders.
	safeRelease(error);
	safeRelease(vertexShader);
	safeRelease(pixelShader);

	// Create vertex buffer for a 3D cube.
	Vertex vertexData[] =
	{
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	};

	const unsigned int vertexDataSize = sizeof(vertexData);

	// Upload the vertex buffer to the GPU using the default heap usage.
	if (FAILED(info->d3dDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(vertexDataSize), D3D12_RESOURCE_STATE_GENERIC_READ,  NULL, IID_PPV_ARGS(&info->d3dVertexBuffer))))
	{
		return false;
	}

	// Upload the vertex buffer to the GPU.
	UINT8* pVertexDataBegin;
	info->d3dVertexBuffer->Map(0, &CD3DX12_RANGE(0, 0), reinterpret_cast<void**>(&pVertexDataBegin));
	memcpy(pVertexDataBegin, vertexData, vertexDataSize);
	info->d3dVertexBuffer->Unmap(0, NULL);

	// Create a vertex buffer view.
	info->d3dvertexBufferView.BufferLocation = info->d3dVertexBuffer->GetGPUVirtualAddress();
	info->d3dvertexBufferView.StrideInBytes = sizeof(Vertex);
	info->d3dvertexBufferView.SizeInBytes = vertexDataSize;

	// 3D vertices box index array.
	WORD indices[] =
	{
		3, 1, 0,
		2, 1, 3,

		0, 5, 4,
		1, 5, 0,

		3, 4, 7,
		0, 4, 3,

		1, 6, 5,
		2, 6, 1,

		2, 7, 6,
		3, 7, 2,

		6, 4, 5,
		7, 4, 6,
	};

	const unsigned int indexDataSize = sizeof(indices);

	// Upload the index buffer to the GPU using the default heap usage.
	if (FAILED(info->d3dDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(indexDataSize), D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&info->d3dIndexBuffer))))
	{
		return false;
	}

	// Upload the index buffer to the GPU.
	UINT8* pIndexDataBegin;
	info->d3dIndexBuffer->Map(0, &CD3DX12_RANGE(0, 0), reinterpret_cast<void**>(&pIndexDataBegin));
	memcpy(pIndexDataBegin, indices, indexDataSize);
	info->d3dIndexBuffer->Unmap(0, NULL);

	// Create a index buffer view.
	info->d3dIndexBufferView.BufferLocation = info->d3dIndexBuffer->GetGPUVirtualAddress();
	info->d3dIndexBufferView.Format = DXGI_FORMAT_R16_UINT;
	info->d3dIndexBufferView.SizeInBytes = indexDataSize;

	// Create matrices for 3D transformation.
	info->worldMatrix = XMMatrixIdentity();
	XMVECTOR eye = XMVectorSet(0.0f, 1.0f, -5.0f, 0.0f);
	XMVECTOR at = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	info->viewMatrix = XMMatrixLookAtLH(eye, at, up);
	info->projectionMatrix = XMMatrixPerspectiveFovLH(XM_PI / 2.5f, 1.0f, 0.01f, 100.0f);

	// Wait for the setup to complete and start the rendering.
	this->WaitForPreviousFrame();
	return true;
}

dx12Render::dx12Render()
{
	// Because we use vectorization (XMMATRIX) we need to align the info struct to a 16-byte boundary.
	originalHeapPtr = HeapAlloc(GetProcessHeap(), 0, sizeof(dx12Info) + 16);
	this->info = (dx12Info*)originalHeapPtr;
	AlignPointer((DWORD_PTR*)&this->info, 16);
	memset(this->info, 0, sizeof(dx12Info));

	// Let's create a heap instance of the Font variable as well.
	//info->ArialFont = new Font();
}

dx12Render::~dx12Render()
{
	// Free the Font variable first.
	//delete info->ArialFont;

	// Free the memory used by the information variables structure.
	HeapFree(GetProcessHeap(), 0, originalHeapPtr);
}

bool dx12Render::init(HWND hWnd)
{
#ifdef _DEBUG
	// Enable the D3D12 debug layer.
	ID3D12Debug* debugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
	{
		debugController->EnableDebugLayer();
	}
#endif

	info->BufferCount = D3D12_BUFFERCOUNT;
	//info->MaxNumTextCharacters = 1024;

	// Get the client rectangle of the window on the desktop.
	RECT rc;
	GetClientRect(hWnd, &rc);
	const UINT width = rc.right - rc.left;
	const UINT height = rc.bottom - rc.top;
	info->d3dViewport.Width = (FLOAT)width;
	info->d3dViewport.Height = (FLOAT)height;
	info->d3dViewport.MinDepth = 0.0f;
	info->d3dViewport.MaxDepth = 1.0f;
	info->d3dViewport.TopLeftX = 0;
	info->d3dViewport.TopLeftY = 0;

	// Calculate the window aspect ratio.
	info->screenAspect = info->d3dViewport.Width / info->d3dViewport.Height;

	// Set scissor rect information to match the viewport.
	info->d3dScissorRect.right = width;
	info->d3dScissorRect.bottom = height;

	// Calculate and save the window clear/background color.
	info->ClearColor[0] = (float)Background.r / 0xFF;
	info->ClearColor[1] = (float)Background.g / 0xFF;
	info->ClearColor[2] = (float)Background.b / 0xFF;
	info->ClearColor[3] = 1.0f;

	// Create DXGI factory to create and use DXGI components later.
	IDXGIFactory4* factory;
#ifdef _DEBUG
	HRESULT h = CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(&factory));
#else
	HRESULT h = CreateDXGIFactory2(0, IID_PPV_ARGS(&factory));
#endif
	if (FAILED(h))
	{
		return false;
	}

	// Create the D3D12 device.
	h = D3D12CreateDevice(NULL, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&info->d3dDevice));
	if (FAILED(h))
	{
		return false;
	}

	// Create a command queue.
	D3D12_COMMAND_QUEUE_DESC queueDesc;
	memset(&queueDesc, 0, sizeof(queueDesc));
	h = info->d3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&info->d3dCommandQueue));
	if (FAILED(h))
	{
		return false;
	}
		
	// Initialize swapchain information using client rectangle.
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
	memset(&swapChainDesc, 0, sizeof(swapChainDesc));

	swapChainDesc.BufferCount = info->BufferCount;
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.Height = height;
	swapChainDesc.Width = width;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	
	// Create the swapchain using the swapchain descriptor.
	IDXGISwapChain1* swapChain;
	h = factory->CreateSwapChainForHwnd(info->d3dCommandQueue, hWnd, &swapChainDesc, NULL, NULL, &swapChain);
	if (FAILED(h))
	{
		return false;
	}

	info->d3dSwapChain = (IDXGISwapChain3*)swapChain;
	info->FrameIndex = info->d3dSwapChain->GetCurrentBackBufferIndex();

	// Create a heap descriptor.
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
	memset(&rtvHeapDesc, 0, sizeof(rtvHeapDesc));
	rtvHeapDesc.NumDescriptors = info->BufferCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	if (FAILED(info->d3dDevice->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&info->d3dRenderTargetViewHeap))))
	{
		return false;
	}

	info->RenderTargetViewDescriptorSize = info->d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	// Create render target views for the each buffer.
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(info->d3dRenderTargetViewHeap->GetCPUDescriptorHandleForHeapStart());
	for (unsigned int i = 0; i < info->BufferCount; ++i)
	{
		info->d3dSwapChain->GetBuffer(i, IID_PPV_ARGS(&info->d3dBackBufferRenderTarget[i]));
		info->d3dDevice->CreateRenderTargetView(info->d3dBackBufferRenderTarget[i], NULL, rtvHandle);
		rtvHandle.Offset(1, info->RenderTargetViewDescriptorSize);
	}
	
	// Create command allocator object.
	if (FAILED(info->d3dDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&info->d3dCommandAllocator))))
	{
		return false;
	}

	// Let's create a fence to synchronize with the GPU.
	for (unsigned int i = 0; i < info->BufferCount; ++i)
	{
		if (FAILED(info->d3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&info->d3dFence[i]))))
		{
			return false;
		}
		info->FenceValue[i] = 1;
	}

	// Create fence parameters and event.
	if (!(info->FenceEvent = CreateEvent(NULL, FALSE, FALSE, NULL)))
	{
		return false;
	}

	// Create a vertex buffer to store the mouse position lines in. We reupload the calculated lines every frame.
	const unsigned int lineVertexDataSize = (4 * sizeof(XMFLOAT3)) + (4 * sizeof(XMFLOAT4));
	if (FAILED(info->d3dDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(lineVertexDataSize), D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&info->d3dLineVerticesBuffer))))
	{
		return false;
	}

	// Create a vertex buffer view for the line vertices.
	info->d3dLinesVertexBufferView.BufferLocation = info->d3dLineVerticesBuffer->GetGPUVirtualAddress();
	info->d3dLinesVertexBufferView.StrideInBytes = sizeof(Vertex);
	info->d3dLinesVertexBufferView.SizeInBytes = lineVertexDataSize;

	// Release resources used by the DXGI factory and root signature blobs.
	safeRelease(factory);

	// Create the root signature for the rendering of objects.
	if (!this->CreateRootSignature())
	{
		return false;
	}

	/*ID3DBlob* error = NULL;
	ID3DBlob* textVertexShader;
	ID3DBlob* textPixelShader;

	// Compile the vertex shader for the text drawing sequence.
#ifdef _DEBUG
	if (FAILED(D3DCompile(vsTextDrawingHLSL, strlen(vsTextDrawingHLSL), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &textVertexShader, &error)) && error)
#else
	if (FAILED(D3DCompile(vsTextDrawingHLSL, strlen(vsTextDrawingHLSL), NULL, NULL, NULL, "VS", "vs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &textVertexShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Compile the pixel shader for the text drawing sequence.
#ifdef _DEBUG
	if (FAILED(D3DCompile(psTextDrawingHLSL, strlen(psTextDrawingHLSL), NULL, NULL, NULL, "PS", "ps_5_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &textPixelShader, &error)) && error)
#else
	if (FAILED(D3DCompile(psTextDrawingHLSL, strlen(psTextDrawingHLSL), NULL, NULL, NULL, "PS", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &textPixelShader, &error)) && error)
#endif
	{
		error->Release();
		return false;
	}

	// Create the text rendering pipeline state object.
	if (!CreateTextRendererPipelineStateObject(info->d3dDevice, textVertexShader, textPixelShader, info->d3dRootSignature, &info->d3dTextPipelineState))
	{
		return false;
	}

	safeRelease(error);
	safeRelease(textPixelShader);
	safeRelease(textVertexShader);*/

	// Load the Arial font information data.
	/*if (!LoadFont(L"Arial.fnt", width, height, info->ArialFont))
	{
		return false;
	}

	// Load the Arial font texture.
	DXGI_FORMAT dxgiFormat;
	if (!CreateTextureResources(info->d3dDevice, info->d3dCommandList, *info->ArialFont, &dxgiFormat))
	{
		return false;
	}*/

	// Create identities and basic 3D cube transformations.
	info->matrices3D.world = XMMatrixIdentity();
	info->matrices3D.projection = XMMatrixIdentity();
	info->matrices3D.view = XMMatrixIdentity();

	D3D12_DESCRIPTOR_HEAP_DESC heapDesc;
	heapDesc.NumDescriptors = 2;
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.NodeMask = 0;

	// Create descriptor heap for the constant buffer and font texture.
	if (FAILED(info->d3dDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&info->d3dMainDescriptorHeap))))
	{
		return false;
	}

	// Create a shader resource view for the font texture data.
	/*D3D12_SHADER_RESOURCE_VIEW_DESC fontsrvDesc;
	memset(&fontsrvDesc, 0, sizeof(fontsrvDesc));
	fontsrvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	fontsrvDesc.Format = dxgiFormat;
	fontsrvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	fontsrvDesc.Texture2D.MipLevels = 1;

	// Create a shader resource view handle to reference while drawing.
	info->SRVHandleSize = info->d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	// Create a shader resource view for the shader units.
	info->ArialFont->SrvHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(info->d3dMainDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), 1, info->SRVHandleSize);
	CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle(info->d3dMainDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), 1, info->SRVHandleSize);

	info->d3dDevice->CreateShaderResourceView(info->ArialFont->TextureBuffer, &fontsrvDesc, srvHandle);*/
	
	// Create frame-buffer specific things.
	for (unsigned int i = 0; i < info->BufferCount; ++i)
	{
		// Create the constant buffer resource. Constant buffer resource heaps must be 64-kb aligned.
		if (FAILED(info->d3dDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(1024 * 64),
			D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&info->d3dMatrixConstantBuffer[i]))))
		{
			return false;
		}

		// Create constant buffer view.
		D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
		cbvDesc.BufferLocation = info->d3dMatrixConstantBuffer[i]->GetGPUVirtualAddress();
		cbvDesc.SizeInBytes = sizeof(MatrixBufferType);
		info->d3dDevice->CreateConstantBufferView(&cbvDesc, info->d3dMainDescriptorHeap->GetCPUDescriptorHandleForHeapStart());

		// Put identity matrices on the GPU.
		UINT8* bufferPointer;
		const CD3DX12_RANGE readRange(0, 0);
		info->d3dMatrixConstantBuffer[i]->Map(0, &readRange, reinterpret_cast<void**>(&bufferPointer));
		memcpy(bufferPointer, &info->matrices3D, sizeof(MatrixBufferType));
		info->d3dMatrixConstantBuffer[i]->Unmap(0, NULL);

		// Create a buffer to hold the text drawing vertices.
		/*if (FAILED(info->d3dDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(info->MaxNumTextCharacters * sizeof(TextVertex)), D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&info->d3dTextVertexBuffer[i]))))
		{
			return false;
		}

		// Map the vertex buffer such that we can directly store vertices there.
		info->d3dTextVertexBuffer[i]->Map(0, &readRange, reinterpret_cast<void**>(&info->TextVertexPointer[i]));

		// Set the text drawing vertex buffer view.
		info->d3dTextVertexBufferView[i].BufferLocation = info->d3dTextVertexBuffer[i]->GetGPUVirtualAddress();
		info->d3dTextVertexBufferView[i].StrideInBytes = sizeof(TextVertex);
		info->d3dTextVertexBufferView[i].SizeInBytes = info->MaxNumTextCharacters * sizeof(TextVertex);*/
	}

	// Start initializing the 2D/3D specific components.
	if (ActiveMode == render2D)
	{
		return this->GFXTestSwitchModeTo2D();
	}

	return this->GFXTestSwitchModeTo3D();
}

void dx12Render::onChangeRenderMode()
{
	// shifting from 2D to 3D, we need to release 2D/3D specific things!
	for (unsigned int i = 0; i < info->BufferCount; ++i)
	{
		// Wait for the GPU to be done with all resources.
		info->FrameIndex = i;
		this->WaitForPreviousFrame();
	}

	// Release rendermode-specific components.
	safeRelease(info->d3dIndexBuffer);
	safeRelease(info->d3dVertexBuffer);
	safeRelease(info->d3dLinesPipelineState);
	safeRelease(info->d3dPipelineState);

	// Switch render mode, reinitializing rendermode-specific components.
	if (ActiveMode == render2D)
	{
		this->GFXTestSwitchModeTo2D();
	}
	else
	{
		this->GFXTestSwitchModeTo3D();
	}
}

// Creates the root signature for the DirectX 12 renderer.
const bool dx12Render::CreateRootSignature() const
{
	// create a root descriptor, which explains where to find the data for this root parameter
	D3D12_ROOT_DESCRIPTOR rootCBVDescriptor;
	rootCBVDescriptor.RegisterSpace = 0;
	rootCBVDescriptor.ShaderRegister = 0;

	// create a descriptor range (descriptor table) and fill it out
	// this is a range of descriptors inside a descriptor heap
	D3D12_DESCRIPTOR_RANGE  descriptorTableRanges[1]; // only one range right now
	descriptorTableRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; // this is a range of shader resource views (descriptors)
	descriptorTableRanges[0].NumDescriptors = 1; // we only have one texture right now, so the range is only 1
	descriptorTableRanges[0].BaseShaderRegister = 0; // start index of the shader registers in the range
	descriptorTableRanges[0].RegisterSpace = 0; // space 0. can usually be zero
	descriptorTableRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND; // this appends the range to the end of the root signature descriptor tables

	// create a descriptor table
	D3D12_ROOT_DESCRIPTOR_TABLE descriptorTable;
	descriptorTable.NumDescriptorRanges = _countof(descriptorTableRanges); // we only have one range
	descriptorTable.pDescriptorRanges = &descriptorTableRanges[0]; // the pointer to the beginning of our ranges array

	// create a root parameter for the root descriptor and fill it out
	D3D12_ROOT_PARAMETER  rootParameters[2]; // two root parameters
	rootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV; // this is a constant buffer view root descriptor
	rootParameters[0].Descriptor = rootCBVDescriptor; // this is the root descriptor for this root parameter
	rootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX; // our pixel shader will be the only shader accessing this parameter for now

	// fill out the parameter for our descriptor table. Remember it's a good idea to sort parameters by frequency of change. Our constant
	// buffer will be changed multiple times per frame, while our descriptor table will not be changed at all (in this tutorial)
	rootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE; // this is a descriptor table
	rootParameters[1].DescriptorTable = descriptorTable; // this is our descriptor table for this root parameter
	rootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL; // our pixel shader will be the only shader accessing this parameter for now

	// create a static sampler
	D3D12_STATIC_SAMPLER_DESC sampler = {};
	sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
	sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler.MipLODBias = 0;
	sampler.MaxAnisotropy = 0;
	sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	sampler.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
	sampler.MinLOD = 0.0f;
	sampler.MaxLOD = D3D12_FLOAT32_MAX;
	sampler.ShaderRegister = 0;
	sampler.RegisterSpace = 0;
	sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	rootSignatureDesc.Init(_countof(rootParameters), // we have 2 root parameters
		rootParameters, // a pointer to the beginning of our root parameters array
		1, // we have one static sampler
		&sampler, // a pointer to our static sampler (array)
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // we can deny shader stages here for better performance
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS);

	ID3DBlob* signature = NULL;
	ID3DBlob* error = NULL;

	// Serialize the root signature data into a blob.
	if (FAILED(D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error)))
	{
		error->Release();
		return false;
	}

	// Create the root signature.
	if (FAILED(info->d3dDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&info->d3dRootSignature))))
	{
		return false;
	}

	return true;
}

// Draws a string using the text renderer components.
/*void dx12Render::DrawString(const wchar_t* string, const unsigned int strlen, const XMFLOAT2 position, XMFLOAT2 scale, XMFLOAT2 padding, XMFLOAT4 color) const
{
	unsigned int numCharacters = 0;

	const float topLeftScreenX = (position.x * 2.0f) - 1.0f;
	const float topLeftScreenY = ((1.0f - position.y) * 2.0f) - 1.0f;

	float x = topLeftScreenX;
	float y = topLeftScreenY;

	float horrizontalPadding = (info->ArialFont->LeftPadding + info->ArialFont->RightPadding) * padding.x;
	float verticalPadding = (info->ArialFont->TopPadding + info->ArialFont->BottomPadding) * padding.y;

	wchar_t lastChar = -1;

	// Set the text pipeline state object to be used for drawing.
	info->d3dCommandList->SetPipelineState(info->d3dTextPipelineState);

	// This way we only need 4 vertices per quad rather than 6 if we were to use a triangle list topology.
	info->d3dCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Set the text vertex buffer.
	info->d3dCommandList->IASetVertexBuffers(0, 1, &info->d3dTextVertexBufferView[info->FrameIndex]);

	// Bind the text SRV. We will assume the correct descriptor heap and table are currently bound and set.
	info->d3dCommandList->SetGraphicsRootDescriptorTable(1, info->ArialFont->SrvHandle);

	// We can directly write vertices to this pointer.
	TextVertex* const vert = (TextVertex*)info->TextVertexPointer[info->FrameIndex];

	// Walk the characters in the string.
	for (unsigned int i = 0; i < strlen; ++i)
	{
		wchar_t c = string[i];

		const FontChar* fc = info->ArialFont->GetChar(c);

		// Character is not present in font char set.
		if (!fc)
		{
			continue;
		}

		// We reached the end of the string.
		if (!c)
		{
			break;
		}

		// We found a new-line character.
		if (c == L'n')
		{
			x = topLeftScreenX;
			y -= (info->ArialFont->LineHeight + verticalPadding) * scale.y;
			continue;
		}

		// don't overflow the buffer. In your app if this is true, you can implement a resize of your text vertex buffer
		if (numCharacters >= info->MaxNumTextCharacters)
		{
			break;
		}

		float kerning = 0.0f;
		if (i > 0)
		{
			kerning = info->ArialFont->GetKerning(lastChar, c);
		}

		// Create a new vertex to be drawn.
		vert[numCharacters++] =
		{
			// The position component.
			{
				x + ((fc->XOffset + kerning) * scale.x),
				y - (fc->YOffset * scale.y),
				fc->Width * scale.x,
				fc->Height * scale.y
			},

			// The texture coordinate structure.
			{
				fc->U,
				fc->V,
				fc->TextureWidth,
				fc->TextureHeight,
			},

			// The color component.
			{
				color.x,
				color.y,
				color.z,
				color.w,
			},
		};

		// Remove the horizontal padding and advance to next character position.
		x += (fc->XAdvance - horrizontalPadding) * scale.x;
		lastChar = c;
	}

	// We are going to have 4 vertices per character (trianglestrip to make quad), and each instance is one character.
	info->d3dCommandList->DrawInstanced(4, numCharacters, 0, 0);
}*/

void dx12Render::frameTick(void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2])
{
	// Reset execution on command allocater.
	info->d3dCommandAllocator->Reset();

	// Reset command list for re-recording of operations for this frame.
	info->d3dCommandList->Reset(info->d3dCommandAllocator, info->d3dPipelineState);

	// Set states and populate command list for frame rendering.
	info->d3dCommandList->SetGraphicsRootSignature(info->d3dRootSignature);
	info->d3dCommandList->RSSetViewports(1, &info->d3dViewport);
	info->d3dCommandList->RSSetScissorRects(1, &info->d3dScissorRect);

	// Indicate that the back buffer will be used as a render target.
	info->d3dCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(info->d3dBackBufferRenderTarget[info->FrameIndex], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	// Set render targets.
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(info->d3dRenderTargetViewHeap->GetCPUDescriptorHandleForHeapStart(), info->FrameIndex, info->RenderTargetViewDescriptorSize);
	info->d3dCommandList->OMSetRenderTargets(1, &rtvHandle, FALSE, NULL);

	// Record actions to be executed during frame rendering.
	info->d3dCommandList->ClearRenderTargetView(rtvHandle, info->ClearColor, 0, NULL);

	// Set primitive topology, should be the same for both 2D and 3D rendering.
	info->d3dCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Set vertex buffer.
	info->d3dCommandList->IASetVertexBuffers(0, 1, &info->d3dvertexBufferView);

	// Set the pipeline state for rendering the primitives.
	info->d3dCommandList->SetPipelineState(info->d3dPipelineState);

	// Set constant buffer / shader resource view descriptor heap.
	ID3D12DescriptorHeap* heaps[] = { info->d3dMainDescriptorHeap };
	info->d3dCommandList->SetDescriptorHeaps(1, heaps);

	// Start drawing the cube / triangle vertices.
	if (ActiveMode == render2D)
	{
		// Draw 2D triangle.
		info->d3dCommandList->DrawInstanced(3, 1, 0, 0);
	}
	else
	{
		// Set constant buffer to be used by shader.
		info->d3dCommandList->SetGraphicsRootConstantBufferView(0, info->d3dMatrixConstantBuffer[info->FrameIndex]->GetGPUVirtualAddress());

		// Set index buffer that contains indices to draw 3D cube.
		info->d3dCommandList->IASetIndexBuffer(&info->d3dIndexBufferView);

		// Calculate the rotation for the 3D vertex box.
		XMMATRIX xRotation;
		info->worldMatrix = XMMatrixRotationY(tick / 400.0f);
		xRotation = XMMatrixRotationX(-0.5f);
		info->worldMatrix = XMMatrixMultiply(info->worldMatrix, xRotation);

		// Set matrices for 3D transformation.
		MatrixBufferType cb;
		cb.world = XMMatrixTranspose(info->worldMatrix);
		cb.view = XMMatrixTranspose(info->viewMatrix);
		cb.projection = XMMatrixTranspose(info->projectionMatrix);

		// Set 3D matrices as constant buffer and draw the indexed 3D box vertices.
		UINT8* bufferPointer;
		const CD3DX12_RANGE readRange(0, 0);
		info->d3dMatrixConstantBuffer[info->FrameIndex]->Map(0, &readRange, reinterpret_cast<void**>(&bufferPointer));
		memcpy(bufferPointer, &cb, sizeof(MatrixBufferType));
		info->d3dMatrixConstantBuffer[info->FrameIndex]->Unmap(0, NULL);

		// Draw 3D cube.
		info->d3dCommandList->DrawIndexedInstanced(36, 1, 0, 0, 0);
	}

	// Create FPS representation string and draw it.
	/*const wchar_t* buf = helper->va(L"fps: %5.02f", fps);
	DrawString(buf, (UINT32)wcslen(buf), XMFLOAT2(0.02f, 0.01f));

	// Draw the input string representations.
	while (gfxInfo && *gfxInfo)
	{
		DrawString(*gfxInfo, (UINT32)wcslen(*gfxInfo), XMFLOAT2(0.02f, 0.01f));
		++gfxInfo;
	}*/
	
	// Set the pipeline state for rendering the mouse position lines.
	info->d3dCommandList->SetPipelineState(info->d3dLinesPipelineState);

	// D3D11 hands a left-handed coordinate system so the mouse position has to be converted to the usable range.
	const float px = ((2.0f * (float)mousePos[0]) / (float)this->Width) - 1.0f;
	const float py = (((2.0f * (float)mousePos[1]) / (float)this->Height) - 1.0f) * -1.0f;

	// I tested this and apparently it runs faster when recreating the whole array every time.
	Vertex lines[] =
	{
		{ XMFLOAT3(-1.0f, py, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, py, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(px, -1.0f, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(px, 1.0f, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	};

	// Upload the line vertices to the GPU.
	UINT8* pVertexDataBegin;
	info->d3dLineVerticesBuffer->Map(0, &CD3DX12_RANGE(0, 0), reinterpret_cast<void**>(&pVertexDataBegin));
	memcpy(pVertexDataBegin, lines, sizeof(lines));
	info->d3dLineVerticesBuffer->Unmap(0, NULL);

	// Set up drawing for the lines.
	info->d3dCommandList->IASetVertexBuffers(0, 1, &info->d3dLinesVertexBufferView);
	info->d3dCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
	info->d3dCommandList->DrawInstanced(4, 1, 0, 0);

	// Indicate that the frame can now be presented.
	info->d3dCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(info->d3dBackBufferRenderTarget[info->FrameIndex], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	// Close the command list.
	info->d3dCommandList->Close();
}

void dx12Render::flip(void*)
{
	// Execute the command list.
	ID3D12CommandList* commandLists[] = { info->d3dCommandList };
	info->d3dCommandQueue->ExecuteCommandLists(_countof(commandLists), commandLists);

	// Present the frame (setting SyncInterval to 1 or higher may fuck up raw input processing).
	info->d3dSwapChain->Present(0, 0);

	// Recheck which buffer is the backbuffer.
	info->FrameIndex = info->d3dSwapChain->GetCurrentBackBufferIndex();

	this->WaitForPreviousFrame();
}

void dx12Render::WaitForPreviousFrame()
{
	// Signal and increment the fence value.
	const unsigned __int64 fence = info->FenceValue[info->FrameIndex]++;
	info->d3dCommandQueue->Signal(info->d3dFence[info->FrameIndex], fence);
	
	// Wait until the previous frame is finished.
	if (info->d3dFence[info->FrameIndex]->GetCompletedValue() < fence)
	{
		info->d3dFence[info->FrameIndex]->SetEventOnCompletion(fence, info->FenceEvent);
		WaitForSingleObject(info->FenceEvent, INFINITE);
	}

	info->FrameIndex = info->d3dSwapChain->GetCurrentBackBufferIndex();
}

void dx12Render::cleanup()
{
	// Release D3D12 resources.
	for (unsigned int i = 0; i < info->BufferCount; ++i)
	{
		// Wait for the GPU to be done with all resources.
		info->FrameIndex = i;
		this->WaitForPreviousFrame();

		safeRelease(info->d3dMatrixConstantBuffer[i]);
		safeRelease(info->d3dFence[i]);
		safeRelease(info->d3dBackBufferRenderTarget[i]);
		//safeRelease(info->d3dTextVertexBuffer[i]);
	}

	CloseHandle(info->FenceEvent);
	safeRelease(info->d3dMainDescriptorHeap);
	safeRelease(info->d3dIndexBuffer);
	safeRelease(info->d3dVertexBuffer);
	safeRelease(info->d3dCommandList);
	safeRelease(info->d3dLineVerticesBuffer);
	safeRelease(info->d3dLinesPipelineState);
	//safeRelease(info->d3dTextPipelineState);
	safeRelease(info->d3dPipelineState);
	safeRelease(info->d3dRootSignature);
	safeRelease(info->d3dCommandAllocator);
	safeRelease(info->d3dRenderTargetViewHeap);
	safeRelease(info->d3dSwapChain);
	safeRelease(info->d3dCommandQueue);
	safeRelease(info->d3dDevice);
}