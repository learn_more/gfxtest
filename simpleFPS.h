#ifndef SIMPLEFPS_H
#define SIMPLEFPS_H

#include <Mmsystem.h>
#pragma comment( lib, "winmm.lib" )

class simpleFPS
{
public:
	simpleFPS() : mFPS(0), mTick(0) {;}
	void init()
	{
		mFPS = 0;
		mTick = this->tick();
		mFrames = 0;
	}
	void frame()
	{
		const float tick_ = tick();
		++mFrames;
		if( (tick_ - mTick) >= 1000 ) {
			mFPS = mFrames * 1000.0f / (tick_ - mTick);
			mTick = tick_;
			mFrames = 0;
		}
	}
	const float fps() const { return mFPS; }
	const float tick() const { return (float)timeGetTime(); }
private:
	unsigned int mFrames;
	float mTick;
	float mFPS;
};


#endif //#ifndef SIMPLEFPS_H
