#ifndef BASEPLUGIN_H
#define BASEPLUGIN_H

// lm: maybe just name them .x64 ?
#if defined(_WIN64)
#define PLUGIN_ARCH		L"64"
#elif defined(_WIN32)
#define PLUGIN_ARCH		L"32"
#else
#error "Invalid arch"
#endif

class pluginHelper
{
public:
	virtual ~pluginHelper() {;}

	virtual void* alloc( size_t size ) = 0;
	virtual void free( void* ) = 0;
	
	virtual void *memset( void *dest, int c, size_t count ) = 0;

	virtual const wchar_t* va( const wchar_t* fmt, ... ) = 0;

};

template<class T>
static void safeRelease( T* &ptr )
{
	if( ptr ) {
		ptr->Release();
		ptr = NULL;
	}
}

class basePlugin
{
public:
	basePlugin() {;}
	virtual ~basePlugin() {;}
	virtual size_t ptr_size() { return sizeof(void*); }
};

typedef basePlugin* (*createProc)( pluginHelper* helper, int pluginVersion );
typedef void (*destroyProc)( basePlugin* render );

#endif //#ifndef BASEPLUGIN_H
