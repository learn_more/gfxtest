#include <Windows.h>
#include "basePlugin.h"

#if defined(RAWINPUT_EXPORTS) || defined(WMINPUT_EXPORTS)
#include "baseInputPlugin.h"
baseInputHelper* helper = NULL;
#else
pluginHelper* helper = NULL;
#endif

#if defined(NDEBUG) && !defined(DX8RENDER_EXPORTS)
#define DX8RENDER_EXPORTS
extern "C"
{
	int _fltused = 0; 
}
#endif

void* operator new (size_t size)
{
	return helper->alloc(size);
}

void* operator new[] (size_t size)
{
	return helper->alloc( size );
}

void operator delete (void* ptr)
{
	helper->free( ptr );
}

void operator delete[] (void* ptr)
{
	helper->free( ptr );
}

int _purecall()
{
	__debugbreak();
	return -1;
}

#pragma function(memset)
extern "C" void* memset(void *target, int fillByte, size_t count)
{
	return helper->memset( target, fillByte, count );
}
