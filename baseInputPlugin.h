#ifndef BASEINPUTPLUGIN_H
#define BASEINPUTPLUGIN_H

#include "basePlugin.h"

const int mbNone = 0;
const int mbLeft = 1;
const int mbRight = 2;
const int mbMiddle = 3;
const int mbButton4 = 4;
const int mbButton5 = 5;


class baseInputHelper : public pluginHelper
{
public:

	virtual void mouseMoveTo( int xPosition, int yPosition ) = 0;
	virtual void mouseMoveAdd( int xAdd, int yAdd ) = 0;
	virtual void mouseButton( int button, bool down ) = 0;

	virtual void keyEvent( long scanCode, bool down ) = 0;
};

#define PLUGININPUT_VERSION		0x04

class baseInput : public basePlugin
{
public:
	enum Type {
		Keyboard = 0,
		Mouse = 1,
		MAX_TYPE
	};


	baseInput() { for( size_t n = 0; n < MAX_TYPE; ++n ) { Capture[n] = true; } }
	virtual ~baseInput() {;}
	
	virtual bool isCapable( Type type ) const = 0;
	void setCapture( Type type, bool enabled )
	{
		if( isCapable( type ) ) {
			Capture[type] = enabled;
			onChangeCapture( type );
		}
	}
	virtual void onChangeCapture( Type ) {;}

	virtual bool init( HWND )
	{
		for( size_t n = 0; n < MAX_TYPE; ++n ) {
			if( Capture[n] && !isCapable( (Type)n ) ) {
				Capture[n] = false;
			}
		}
		return true;
	}
	virtual LRESULT processMessage( HWND, UINT, WPARAM, LPARAM, int& ) { return 0; }

	virtual void cleanup() {;}

	bool Capture[MAX_TYPE];
};

#endif //#ifndef BASEINPUTPLUGIN_H
