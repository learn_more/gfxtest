#ifndef OGLRENDER_H
#define OGLRENDER_H

#include "../baseRenderPlugin.h"

struct oglInfo;

class oglRender : public baseRender
{
public:
	oglRender();
	virtual ~oglRender();

	virtual bool isCapable( RenderMode ) const { return true; }
	virtual void onChangeRenderMode();

	virtual bool preInit( WNDCLASSEX& wcex, RenderMode initialMode );
	virtual bool init( HWND hWnd );
	
	virtual void frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] );
	virtual void flip( void* );
	
	virtual void cleanup();

private:
	oglRender& operator = ( const oglRender& );
	oglInfo& info;
};


#endif //#ifndef OGLRENDER_H