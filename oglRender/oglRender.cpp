#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <Shlwapi.h>
#include "oglRender.h"

#pragma comment(lib, "Shlwapi.lib")

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#include <GL/gl.h>
#include <GL/glu.h>

extern pluginHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createRender( pluginHelper* help, int version )
{
	if( PLUGINRENDER_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new oglRender();
}

extern "C" __declspec(dllexport)
void destroyRender( basePlugin* render )
{
	delete render;
}

struct oglInfo
{
	HDC	hdc;
	HGLRC hrc;
	HWND hwnd;

	float fontHeight;
	float Alpha;
};

GLfloat vertices[] =
{
	-1, -1, -1,   -1, -1,  1,   -1,  1,  1,   -1,  1, -1,
	 1, -1, -1,    1, -1,  1,    1,  1,  1,    1,  1, -1,
	-1, -1, -1,   -1, -1,  1,    1, -1,  1,    1, -1, -1,
	-1,  1, -1,   -1,  1,  1,    1,  1,  1,    1,  1, -1,
	-1, -1, -1,   -1,  1, -1,    1,  1, -1,    1, -1, -1,
	-1, -1,  1,   -1,  1,  1,    1,  1,  1,    1, -1,  1
};

GLfloat colors[] =
{
	0, 0, 0,   0, 0, 1,   0, 1, 1,   0, 1, 0,
	1, 0, 0,   1, 0, 1,   1, 1, 1,   1, 1, 0,
	0, 0, 0,   0, 0, 1,   1, 0, 1,   1, 0, 0,
	0, 1, 0,   0, 1, 1,   1, 1, 1,   1, 1, 0,
	0, 0, 0,   0, 1, 0,   1, 1, 0,   1, 0, 0,
	0, 0, 1,   0, 1, 1,   1, 1, 1,   1, 0, 1
};

typedef BOOL(APIENTRY *PFNWGLSWAPINTERVALFARPROC)(int);
PFNWGLSWAPINTERVALFARPROC wglSwapIntervalEXT =	NULL;

// Used to manually switch off swap intervals (vsync).
void TurnOffVertivalSync()
{
	const char *extensions = (char*)glGetString(GL_EXTENSIONS);
	if (StrStrA(extensions, "WGL_EXT_swap_control") == 0)
	{
		return; // Error: WGL_EXT_swap_control extension not supported on your computer.
	}
	else
	{
		// Find the function which turns off swap interval.
		wglSwapIntervalEXT = (PFNWGLSWAPINTERVALFARPROC)wglGetProcAddress("wglSwapIntervalEXT");
		if (wglSwapIntervalEXT)
		{
			wglSwapIntervalEXT(0);
		}
	}
}

oglRender::oglRender()
:info(*(new oglInfo()))
{
	memset( &info, 0, sizeof(oglInfo) );
	info.Alpha = 0.f;
	info.fontHeight = 0.f;
}

oglRender::~oglRender()
{
	delete &info;
}

bool oglRender::preInit( WNDCLASSEX& wcex, RenderMode initialMode )
{
	wcex.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	return baseRender::preInit( wcex, initialMode );
}

bool oglRender::init( HWND hWnd )
{
	info.hwnd = hWnd;
	info.hdc = GetDC( hWnd );
	PIXELFORMATDESCRIPTOR pfd = { sizeof(pfd), 0 };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;

	int pf = ChoosePixelFormat( info.hdc, &pfd);
	SetPixelFormat( info.hdc, pf, &pfd);

	info.hrc = wglCreateContext( info.hdc );
	wglMakeCurrent( info.hdc, info.hrc );
	wglUseFontBitmaps( info.hdc, 0, 256, 1000 );

	TEXTMETRIC tm; 
	GetTextMetrics( info.hdc, &tm ); 
	info.fontHeight = (float)(tm.tmHeight + tm.tmExternalLeading);

	// Turn off vertical sync manually.
	TurnOffVertivalSync();

	onChangeRenderMode();
	SwapBuffers( info.hdc );
	return true;
}

void oglRender::onChangeRenderMode()
{
	glPopMatrix();
	glPushMatrix();
	glClearColor( Background.r/255.0f, Background.g/255.0f, Background.b/255.0f, 1 );
	if( ActiveMode == render2D ) {
		glDisable( GL_DEPTH_TEST );
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		gluOrtho2D( 0, Width, Height, 0 );
		//glClear( GL_COLOR_BUFFER_BIT );
	} else {
		glEnable( GL_DEPTH_TEST );
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		gluPerspective( 20, Width / (float) Height, 5, 15 );
		glViewport( 0, 0, Width, Height );
		glMatrixMode( GL_MODELVIEW );
	}
}

void oglRender::frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] )
{
	if( ActiveMode == render2D ) {
		glClear( GL_COLOR_BUFFER_BIT );
		glBegin( GL_TRIANGLES );
			glColor3d( Object.r, Object.g, Object.b );
			glVertex2i( Width/2, Offset );
			glVertex2i( Offset, Height-Offset );
			glVertex2i( Width-Offset, Height-Offset );
		glEnd();
	} else {
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix();
		glLoadIdentity();
		glTranslatef( 0, 0, -10 );
		glRotatef( 30, 1, 0, 0 );
		glRotatef( info.Alpha, 0, -1, 0 );
		glEnableClientState( GL_VERTEX_ARRAY );
		glEnableClientState( GL_COLOR_ARRAY );
		glVertexPointer( 3, GL_FLOAT, 0, vertices );
		glColorPointer( 3, GL_FLOAT, 0, colors );
		glDrawArrays(GL_QUADS, 0, 24);
		glDisableClientState( GL_COLOR_ARRAY );
		glDisableClientState( GL_VERTEX_ARRAY );

		info.Alpha = tick / 4.f;
		glPopMatrix();
	}

	bool was2d = ActiveMode == render2D;
	if( !was2d ) {
		setRenderMode( render2D );
	}

	glColor3d( 255, 255, 255 );
	float y = 20;
	glRasterPos2f( 10, y );
	glPushAttrib(GL_LIST_BIT);
	glListBase( 1000 );
	const wchar_t* buf = helper->va( L"fps: %5.02f", fps );
	glCallLists( (GLsizei)wcslen(buf), GL_UNSIGNED_SHORT, buf );
	while( gfxInfo && *gfxInfo ) {
		y += info.fontHeight;
		glRasterPos2f( 10, y );
		glCallLists((GLsizei)wcslen(*gfxInfo), GL_UNSIGNED_SHORT, *gfxInfo);
		++gfxInfo;
	}
	glPopAttrib();

	glBegin( GL_LINES );
		glColor3d(0, 0, 0);
		glVertex2i( mousePos[0], 0 );
		glVertex2i( mousePos[0], Height );
		glVertex2i( 0, mousePos[1] );
		glVertex2i( Width, mousePos[1] );
	glEnd();

	if( !was2d ) {
		setRenderMode( render3D );
	}
}

void oglRender::flip( void* )
{
	SwapBuffers( info.hdc );
}

void oglRender::cleanup()
{
	wglMakeCurrent( NULL, NULL );
	ReleaseDC( info.hwnd, info.hdc );
	wglDeleteContext( info.hrc );
}
