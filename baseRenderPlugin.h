#ifndef BASERENDERPLUGIN_H
#define BASERENDERPLUGIN_H

#include "basePlugin.h"

#define PLUGINRENDER_VERSION	0x05

struct Color
{
	BYTE r, g, b;

	Color() : r(0), g(0), b(0) {;}
	Color( BYTE c ) : r(c), g(c), b(c) {;}
	Color( BYTE rr, BYTE gg, BYTE bb ) : r(rr), g(gg), b(bb) {;}
};

struct ResolutionDesc
{
	LONG w;
	LONG h;
};

class baseRender : public basePlugin
{
public:
	enum RenderMode {
		render2D,
		render3D,
	};


	baseRender() : Width( 640 ), Height(480), Offset(40), Background( 255, 0, 255 ), Object( 255, 0, 0 ), ActiveMode(render2D), UseWMPaint(false) {;}
	virtual ~baseRender() {;}
	
	virtual bool isCapable( RenderMode mode ) const = 0;
	void setRenderMode( RenderMode mode )
	{
		if( isCapable( mode ) ) {
			ActiveMode = mode;
			onChangeRenderMode();
		}
	}
	virtual void onChangeRenderMode() {;}
	virtual bool preInit( WNDCLASSEX&, RenderMode initialMode )
	{
		if( isCapable( initialMode ) ){
			ActiveMode = initialMode;
			return true;
		}
		return false;
	}
	virtual bool init( HWND hWnd ) = 0;

	virtual void* modifyData( void* data )
	{
		return data;		//directx plugins can return dx device instead of data f.e.
	}

	virtual void frameTick( void* data, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] ) = 0;
	virtual void flip( void* data ) = 0;

	virtual void cleanup() = 0;

	LONG Width, Height, Offset;
	Color Background;
	Color Object;
	RenderMode ActiveMode;
	bool UseWMPaint;
};

#endif //#ifndef BASERENDERPLUGIN_H
